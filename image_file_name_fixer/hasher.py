import hashlib

"""
This module provides hashing functions.
"""


class Hasher:
    """
    This class provides hashing functions.
    """
    def __init__(self):
        pass

    @staticmethod
    def hash_file(file_path):
        """
        Given a file path, compute the sha256 hash sum for that file.

        :param file_path: Full path to file.  String value.
        :returns: The file's sha256 hash sum.  String value.
        """
        # if file_path does not exist, exception thrown, pass
        # up to caller.
        with open(file_path, "rb") as file_handle:
            data = file_handle.read()
        return hashlib.sha256(data).hexdigest()
